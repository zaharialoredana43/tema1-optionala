
const FIRST_NAME = "Zaharia";
const LAST_NAME = "Loredana";
const GRUPA = "1085";

/**
 * Make the implementation here
 */

function numberParser(a)
{
    if(a===NaN)
    return NaN;
    if(a===Infinity||a===Infinity)
    return NaN;
    if(a>Number.MAX_SAFE_INTEGER||a<Number.MIN_SAFE_INTEGER)
    return NaN;
    
    return Number.parseInt(a);
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

